import pathlib

import setuptools

_my_dir = pathlib.Path(__file__).parent
with open(_my_dir.parent / 'README.md', "r", encoding="utf-8") as f:
    long_description = f.read()

setuptools.setup(
    name="Workplaces",
    version="0.0.1",
    author="Bas van 't Hof for VORtech Computing",
    author_email="bas.vanthof@vortech.nl",
    description="workplaces: an assignment for the course Scientific Software Engineering",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/VORtechBV/assignment_workspaces.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    install_requires=[
        "coverage~=6.4.4",
        "json-schema-for-humans~=0.41.8",
        "jsonschema~=4.15.0",
        "matplotlib~=3.5.3",
        "mypy~=0.971",
        "pycodestyle~=2.9.1",
        "pyflakes~=2.5.0",
        "PyQt5~=5.15.7",
        "pytest~=7.1.2",
        "PyYAML~=6.0",
        "simplejson~=3.17.6",
    ],
)
