.. _tests-label:

Tests for the work_places system
================================

Test **test_main**
------------------
Test **test_main** runs an optimization for which the correct answer is known. 

Even when the whole problem is rotated, the answer is essentially the same (after rotating the answers back), and this is tested too.


.. automodule:: workplaces.test_main
   :members:

Test **test_office_space**
--------------------------
Test **test_office_space** tests the class OfficeSpace.

.. automodule:: workplaces.test_office_space
   :members:

Test **test_work_places**
--------------------------
Test **test_work_places** tests the class WorkPlaces.

.. automodule:: workplaces.test_work_places
   :members:

