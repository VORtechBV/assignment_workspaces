.. workspaces documentation master file, created by
   sphinx-quickstart on Mon Aug 29 16:47:11 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to workspaces's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   main
   office_space
   work_places
   schema_tools
   tests

The main program is described in chapter :ref:`main-program-label`.

It uses two important classes: :ref:`office-space-label` and :ref:`work-places-label`.

The input is given in the form of a yaml-file, for which a yaml-schema is available.

This schema is used to validate the input file, and also to fill in default values for all data that was not filled in by the user.
These tools are described in section :ref:`schema-tools-label`.

Some tests are available, which can be run using `pytest <https://docs.pytest.org/>`_. 
These tests are discussed in Section :ref:`tests-label`


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
