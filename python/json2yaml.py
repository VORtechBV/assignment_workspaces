#!/usr/bin/env python3

import argparse
import json
import pathlib
from datetime import datetime, timezone

import yaml


puff_datetime_fmt = r'%Y-%m-%dT%H:%M:%S'

def _main():
    parser = argparse.ArgumentParser(description="Convert YAML files to JSON or JSON files to YAML")
    parser.add_argument('src_file', type=_existing_file)
    parser.add_argument('dest_file', type=pathlib.Path)
    args = parser.parse_args()

    convert(args.src_file, args.dest_file)


def convert(src: pathlib.Path, dest: pathlib.Path):
    target, dump_args = {
        '.yml': (yaml, dict()),
        '.json': (json, dict(ensure_ascii=True, default=_str_datetime_as_UTC)),
    }[dest.suffix]

    yaml_object = load_file_as_dict(src)

    with open(dest, 'w') as f:
        target.dump(yaml_object, f, indent=3, sort_keys=False, **dump_args)


def load_file_as_dict(filepath: pathlib.Path):
    loader = {
        '.json': json.load,
        '.yml': yaml.safe_load,
    }[filepath.suffix]

    with open(filepath, 'r') as f:
        return loader(f)  # type: ignore


def _str_datetime_as_UTC(dt):
    if not isinstance(dt, datetime):
        raise ValueError()

    is_aware = (dt.tzinfo is not None) and (dt.tzinfo.utcoffset(dt) is not None)
    if is_aware:
        utc_dt = dt.astimezone(tz=timezone.utc).replace(tzinfo=None)
    else:
        utc_dt = dt
    return utc_dt.strftime(puff_datetime_fmt)


def _existing_file(s):
    p = pathlib.Path(s)
    if not p.is_file():
        raise ValueError(f'This is not a file: {p.resolve()}')

    if p.suffix not in ('.yml', '.json'):
        raise ValueError(f"This file does not have a proper extension, so I am cowardly refusing to parse it: {p}")

    return p


if __name__ == '__main__':
    _main()
