import matplotlib.pyplot as plt
import numpy as np

from workplaces.util import distance_method


class OfficeSpace(object):
    """
    Describe the office space as a 2D shape.

    The office has the following properties:

    + **measure**: EUCLIDEAN or MANHATTAN, indicating how distances should be measured in the office;
    + **contours**: shape of the office
    """

    def __init__(self, inputs, dx=1, dy=1):
        self.measure = (distance_method.EUCLIDEAN if inputs['measure'] == 'EUCLIDEAN'
                        else distance_method.MANHATTAN)
        self._xy = np.array(inputs['contours'])
        self._dxy = [dx, dy]

    def check_inside(self, x_grid, y_grid):
        """
        Check whether locations (x_grid, y_grid) are inside the office
        """
        x = self._xy[:, 0]
        y = self._xy[:, 1]

        n_crossings = np.zeros(x_grid.shape)
        for ixy in np.arange(1, len(x)):

            xstart = x[ixy-1]
            ystart = y[ixy-1]

            xend = x[ixy]
            yend = y[ixy]

            det = ystart-yend
            if np.abs(det) > 1e-10:
               alpha = ((ystart-yend) * (xstart - x_grid) + (xend-xstart)*(ystart-y_grid))/det
               beta = (ystart-y_grid) / det
               n_crossings[(alpha > 0) & (beta > 0) & (beta < 1)] += 1

        return (n_crossings % 2) == 1

    def getgrid(self):
        """
        Return x,y coordinates that define a grid on the office floor
        """
        x = self._xy[:, 0]
        y = self._xy[:, 1]
        x_grid, y_grid = np.meshgrid(np.arange(np.min(x)+self._dxy[0]/2, np.max(x), self._dxy[0]),
                                     np.arange(np.min(y)+self._dxy[1]/2, np.max(y), self._dxy[1]))
        return x_grid, y_grid

    def plot(self, plot_grid=False):
        """ Plot the office (and the grid) in the current figure """
        plt.plot(self._xy[:, 0], self._xy[:, 1], 'k')
        if plot_grid:
            x, y = self.getgrid()
            Inside = self.check_inside(x, y)
            x = x.astype(np.double)
            x[np.logical_not(Inside)] = np.NaN
            plt.plot(x, y, 'b.')

    def rotate(self, angle):
        """ Rotate the office contour by the given angle [rad]"""
        cosangle = np.cos(angle)
        sinangle = np.sin(angle)
        x = cosangle * self._xy[:, 0] + sinangle * self._xy[:, 1]
        y = -sinangle * self._xy[:, 0] + cosangle * self._xy[:, 1]
        self._xy[:, 0] = x
        self._xy[:, 1] = y

    def print(self):
        """ Print some information about the current OfficeSpace object"""
        print('  measure: '+('EUCLIDEAN' if self.measure == distance_method.EUCLIDEAN else 'MANHATTAN'))
        print(' sampling: (%0.2g,%0.2g) [m]' % (self._dxy[0], self._dxy[1]))
        print('   contours:')
        for i in range(self._xy.shape[0]):
            print('          (%0.2g,%0.2g)' % (self._xy[i, 0], self._xy[i, 1]))
