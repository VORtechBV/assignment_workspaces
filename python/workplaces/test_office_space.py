#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
from workplaces.util import workplaces_parser
from workplaces.office_space import OfficeSpace

XY = [[0, 0], [10, 0], [10, 16], [0, 16], [3, 8], [0, 0]]
CORRECT_AREA = 136.0


def test_office_space(do_plot=False):
    """
    This test program tests the functions getgrid and check_inside of the class OfficeSpace.

    To do this, an office is defined with a known area, equal to 136 m2.

    For a series of grid sizes, the area is approximated by adding the areas of grid cells inside
    the office.

    The calculated area is compared with an error tolerance that is reasonable given the grid sizes.
    """
    for i in range(1, 7):
        dx = 0.43 * i
        dy = 0.37 * i
        space = OfficeSpace({"measure": 'EUCLIDEAN', 'contours': XY}, dx=dx, dy=dy)
        x_grid, y_grid = space.getgrid()
        inside = space.check_inside(x_grid, y_grid)
        n_inside = np.sum(inside)
        area = dx*dy*n_inside

        print('(dx,dy)=(%g0.3,%g0.3): (nx,ny)=(%d,%d),' %
              (dx, dy, x_grid.shape[0], x_grid.shape[1]),
              '%d points inside the office, office = %0.3g m2' %
              (n_inside, area))

        acceptable_error = dx * 16 + dy * 10
        print('error in area = %0.3g' % ((area - CORRECT_AREA)/acceptable_error),
              'times the acceptable error')
        if abs(area-CORRECT_AREA) > acceptable_error:
            raise RuntimeError('error is too large')

        if do_plot:
            space.plot()
            plt.show()


if __name__ == '__main__':
    parser = workplaces_parser(test_office_space)
    parser.add_argument("-p", "--show_plot", type=int,
                        help="0:no plots, 1:plot for coarse grid, 2:all plots", default=0)
    args = parser.parse_args()

    test_office_space(args.show_plot >= 1)
