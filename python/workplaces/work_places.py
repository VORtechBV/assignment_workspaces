import matplotlib.pyplot as plt
import numpy as np

from workplaces.util import distance, reduction_method


class WorkPlaces(object):
    """
    Describe the work places of the workers in an office.

    Each work place has

    + the name of whose spot it is;
    + (x,y) coordinates
    + information about how often each person drinks cofee or goes to toilet
    """

    def __init__(self, workplaces):
        self._places = workplaces

    def add_place(self, place):
        self._places.append(place)

    def _walking_distance(self, xy_toilet, xy_coffee, reduction, dist_method):
        """ Calculate the distance walked by all the workers in a day
            Return the minimum/average value, depending on the chosen reduction"""

        retval = 1e30 if reduction == reduction_method.MINIMUM else 0

        for place in self._places:
            dist = (
                2.0/3.0 * place['coffee_frequency'] 
                * 2 * distance(xy_coffee[0], xy_coffee[1], place['x'], place['y'], dist_method)
                + 1.0/3.0 * place['coffee_frequency'] 
                * (distance(xy_coffee[0], xy_coffee[1], place['x'], place['y'], dist_method) +
                   distance(xy_coffee[0], xy_coffee[1], xy_toilet[0], xy_toilet[1], dist_method) +
                   distance(xy_toilet[0], xy_toilet[1], place['x'], place['y'], dist_method)))
            if reduction == reduction_method.AVERAGE:
                retval += dist / len(self._places)
            if reduction == reduction_method.MINIMUM:
                retval = np.minimum(retval, dist)
            else:
                raise ValueError('unknown reduction method')

        return retval

    def shortest_path(self, xy_toilet, xy_coffee, dist_method):
        """ Calculate the shortest distance distance walked per day by any of the the workers """
        return self._walking_distance(xy_toilet, xy_coffee,
                                      reduction=reduction_method.MINIMUM, dist_method=dist_method)

    def average_distance(self, xy_toilet, xy_coffee, dist_method):
        """ Calculate the average distance distance walked per day by the workers """
        return self._walking_distance(xy_toilet, xy_coffee,
                                      reduction=reduction_method.AVERAGE, dist_method=dist_method)

    def plot(self):
        """ Plot cyan stars at the work places in the current figure """
        for place in self._places:
            plt.plot(place['x'], place['y'], 'c*')

    def rotate(self, angle):
        """ Rotate the locations of the workers by the given angle [rad]"""
        cosangle = np.cos(angle)
        sinangle = np.sin(angle)
        for place in self._places:
            x = cosangle * place['x'] + sinangle * place['y']
            y = -sinangle * place['x'] + cosangle * place['y']
            place['x'] = x
            place['y'] = y

    def print(self):
        """ Print some information about this WorkPlaces object """
        for place in self._places:
            print('    ' + place['name'] + ': (%0.2g,%0.2g)' % (place['x'], place['y']))
