#!/usr/bin/env python3
from enum import Enum

import numpy as np
import argparse

SCHEMA_FNAME = 'files/workplaces.schema.yml'

distance_method = Enum('distance_method',  ['EUCLIDEAN', 'MANHATTAN'])

reduction_method = Enum('reduction_method', ['MINIMUM',   'AVERAGE'])

class Formatter(argparse.ArgumentDefaultsHelpFormatter,     argparse.RawDescriptionHelpFormatter):
    pass

def workplaces_parser(fun):
    return argparse.ArgumentParser(description=fun.__doc__,  formatter_class=Formatter)

def distance(x1, y1, x2, y2, method=distance_method.MANHATTAN):
    """
    Calculate the distance between (x1,y1) and (x2,y2)
    according to the chosen method (euclidean or manhattan)

    :parameter np.array x1: x coordinate of the first points
    :parameter np.array y1: y coordinate of the first points
    :parameter np.array x2: x coordinate of the second points
    :parameter np.array y2: y coordinate of the second points
    :parameter enumerate method: method for computing distances, either EUCLIDEAN or MANHATTAN

    return np.array of absolute distances for all the points
    """
    if method == distance_method.EUCLIDEAN:
        return np.sqrt((x1-x2)**2 + (y1-y2)**2)
    if method == distance_method.MANHATTAN:
        return np.abs(x1-x2) + np.abs(y1-y2)
    else:
        raise ValueError("method should be MANHATTAN or EUCLIDEAN, not %s\n", method)
