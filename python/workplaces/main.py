#!/usr/bin/env python3
import argparse

from workplaces.office_space import OfficeSpace
from workplaces.optimize import optimize_workspace, show_shortest_path
from workplaces.util import SCHEMA_FNAME, workplaces_parser
from workplaces.validate_and_defaults import read_input_with_defaults
from workplaces.work_places import WorkPlaces


def main(fname):
    """
    This program aims to help solve a problem which exists in every office
    environment: people do not move enough. The only reasons why they get off
    their chairs is to get coffee or to go to the toilet.

    The program models the office workers' behavior, and then chooses an
    optimal location for the coffee machine and for the toilet.

    The optimal locations are defined by the distances walked by the people in
    the office, specifically the person who walks the __least__ of all the
    people in the office.  The optimal location makes this 'shortest distance'
    is as long as possible.

    This program reads the input file, which is a yaml file for which a yaml
    schema is available.  See the generated documentation for information about
    the syntax of the input file.

    The program

    + creates the objects ( **office** and **places** ) representing the office
      and the people in it.
    + Chooses the location for toilet and coffee machine.  The location is
      chosen by trying all possible combinations of 2 locations in a 2D grid of
      the office, and is therefore not computationally efficient.
    + creates a plot to visualize the result.
    """

    inputs = read_input_with_defaults(fname, SCHEMA_FNAME)
    office = OfficeSpace(inputs['office'])
    workplaces = WorkPlaces(inputs['workplaces'])
    xy_coffee, xy_toilet, best = optimize_workspace(office, workplaces)
    show_shortest_path(office, workplaces, xy_coffee, xy_toilet)


if __name__ == '__main__':
    parser = workplaces_parser(main)
    parser.add_argument('inputfile', nargs=argparse.REMAINDER, help="input file (*.yml)")
    args = parser.parse_args()
    print(args)
    main(args.inputfile[0])
