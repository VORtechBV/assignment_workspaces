#!/usr/bin/env python3
import numpy as np

from workplaces.office_space import OfficeSpace
from workplaces.optimize import optimize_workspace, show_shortest_path
from workplaces.util import SCHEMA_FNAME, workplaces_parser
from workplaces.validate_and_defaults import read_input_with_defaults
from workplaces.work_places import WorkPlaces

NANGLE = 7
INPUT_FILE = 'files/invoer.workplaces.yml'

XY_COFFEE_REF = [30, 40]
XY_TOILET_REF = [0, 0]
BEST_REF = 164


def test_optimize(show_plot=0):
    """
    This program tests the optimization algorithm of program workspaces.

    The test runs a calculation in a variety of rotation angles and grid sizes.
    For each rotation angle, the sample input file is read, and then its contents are rotated
    The optimization is performed for a series of grid sizes, and every time, the
    optimal shortest distance is compared to the correct answer.
    Also the optimal locations for coffee machine and toilet are compared to the correct answer,
    after they have been rotated back to correspond to the original inputs.

    The answers are compared to the reference with an error tolerance that is reasonable given the grid sizes.
    """

    for iangle in range(NANGLE+1):
        angle = 2 * np.pi * iangle/NANGLE

        # Construct the objects
        inputs = read_input_with_defaults(INPUT_FILE, SCHEMA_FNAME)
        office = OfficeSpace(inputs['office'])
        places = WorkPlaces(inputs['workplaces'])

        # Rotate the objects
        office.rotate(angle)
        places.rotate(angle)

        # Print the objects
        print('\nwork places:')
        places.print()
        print('\noffice:')
        office.print()
        print('')

        dx = 1.5
        dy = 1.7

        for i in range(1, 5):

            dx = dx / 1.3
            dy = dy / 1.3

            print('\ndx,dy=(%0.4g,%0.4g))' % (dx, dy))
            office._dxy = [dx, dy]

            # Optimize and show
            xy_coffee, xy_toilet, best = optimize_workspace(office=office, workplaces=places)
            print('The best place for the coffee machine is (%0.4g,%0.4g)' % (xy_coffee[0], xy_coffee[1]))
            print('The best place for the toilet         is (%0.4g,%0.4g)' % (xy_toilet[0], xy_toilet[1]))

            if (show_plot >= 1 and i == 1) or show_plot >= 2:
                show_shortest_path(office, places, xy_coffee, xy_toilet)

            print('coffee, toilet error = %19.2e, %10.2e' % (
                abs(XY_COFFEE_REF[0]-xy_coffee[0]) + abs(XY_COFFEE_REF[1]-xy_coffee[1]),
                abs(XY_TOILET_REF[0]-xy_toilet[0]) + abs(XY_TOILET_REF[1]-xy_toilet[1])))

            print("best = ", best)
            print('shortest_path = %0.3g, error = %0.4g' % (best, (best-BEST_REF)/(dx + dy)**2))

            # Rotate the answers back
            cosangle, sinangle = np.cos(angle), np.sin(angle)
            xy_coffee = [cosangle * xy_coffee[0] - sinangle * xy_coffee[1],
                         sinangle * xy_coffee[0] + cosangle * xy_coffee[1]]

            if (np.maximum(np.abs(XY_COFFEE_REF[0]-xy_coffee[0]) + abs(XY_COFFEE_REF[1]-xy_coffee[1]),
                           np.abs(XY_TOILET_REF[0]-xy_toilet[0]) + abs(XY_TOILET_REF[1]-xy_toilet[1]))
                    > 1.1*(dx + dy)):
                print('coffee, toilet locations are too far from correct answer',
                      np.abs(XY_COFFEE_REF[0]-xy_coffee[0]), abs(XY_COFFEE_REF[1]-xy_coffee[1]),
                      np.abs(XY_TOILET_REF[0]-xy_toilet[0]), abs(XY_TOILET_REF[1]-xy_toilet[1]))
                raise RuntimeError('incorrect result')

            if abs(best-BEST_REF) > (dx+dy)**2:
                print('shortest path error: ', best, '!=', BEST_REF)
                raise RuntimeError('incorrect result')


if __name__ == '__main__':

    parser = workplaces_parser(test_optimize)
    parser.add_argument("-p", "--show_plot", type=int,
                        help="0:no plots, 1:plot for coarse grid, 2:all plots", default=0)
    args = parser.parse_args()
    test_optimize(args.show_plot)
