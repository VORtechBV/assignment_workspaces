import json
import pathlib

import yaml
from jsonschema import validate


def reference(schema, ref):
    words = ref.split('/')
    if words.pop(0) != '#':
        raise RuntimeError("reference should start with '#'")
    retval = schema
    while words:
        retval = retval[words.pop(0)]
    return retval


def defaults_now_at(schema, schema_now_at, input_now_at):
    verbose = False
    if verbose:
        print("Filling in defaults for ", json.dumps(input_now_at, indent=3))
        print("with schema", json.dumps(schema_now_at, indent=3))

    if 'properties' not in schema_now_at:
        prop_type = schema_now_at['type']
        if prop_type == 'array':
            for item in input_now_at:
                defaults_now_at(schema, schema_now_at['items'], item)
        elif prop_type == 'number':
            pass
        else:
            raise RuntimeError('kan ik nog niet')
    else:
        for prop in schema_now_at['properties']:
            if '$ref' in schema_now_at['properties'][prop]:
                schema_next_at = reference(schema, schema_now_at['properties'][prop]['$ref'])
            else:
                schema_next_at = schema_now_at['properties'][prop]
            if verbose:
                print("Filling in defaults for property", prop)
                print("with schema", json.dumps(schema_next_at, indent=3))
            prop_type = schema_next_at['type']

            if prop not in input_now_at:
                input_now_at[prop] = schema_next_at['default']
            elif prop_type == 'array':
                if '$ref' in schema_next_at['items']:
                    ref = reference(schema, schema_next_at['items']['$ref'])
                    for item in input_now_at[prop]:
                        defaults_now_at(schema, ref, item)
                else:
                    for item in input_now_at[prop]:
                        defaults_now_at(schema, schema_next_at['items'], item)
            elif prop_type == 'object':
                defaults_now_at(schema, schema_next_at, input_now_at[prop])
            elif prop_type not in ['string', 'number']:
                print(json.dumps(schema_next_at, indent=3))
                raise RuntimeError('kan ik nog niet: ' + prop_type)


def preprocess_input_file(input_fname, schema_fname):
    with open(schema_fname, 'r') as file:
        schema = yaml.safe_load(file)
    with open(input_fname, 'r') as file:
        input_dict = yaml.safe_load(file)
    validate(input_dict, schema)
    with open(str(input_fname) + '.without.defaults.yml', 'w') as file:
        yaml.dump(input_dict, file)

    schema_now_at = reference(schema, schema['$ref'])
    input_now_at = input_dict
    defaults_now_at(schema, schema_now_at, input_now_at)

    with open(str(input_fname) + '.with.defaults.yml', 'w') as file:
        yaml.dump(input_dict, file)


def fill_in_defaults(input_dict, schema):
    schema_now_at = reference(schema, schema['$ref'])
    input_now_at = input_dict
    defaults_now_at(schema, schema_now_at, input_now_at)


def dump_json(dict, fname):
    fname = pathlib.Path(fname).resolve()
    if fname.exists():
        print(f'overwriting {fname}')
    with open(fname, 'w') as f:
        json.dump(dict, f, indent=3, sort_keys=False, ensure_ascii=True)


def read_input_with_defaults(fname, schema_fname):
    """
    Validate the contents of the input file with the schema.
    Then, fill in the defaults for all missing values and return the inputs in a dict.

    During processing, yaml-representations of the input with and without the defaults are
    dumped. This allows the user to compare the two and detect which defaults have been
    filled in.
    """
    fname = pathlib.Path(fname).resolve()
    schema_fname = pathlib.Path(schema_fname).resolve()

    try:
        with open(schema_fname, 'r') as file:
            schema = yaml.safe_load(file)
    except Exception as e:
        raise Exception("Could not read file %s: %s" % (schema_fname, e))

    try:
        with open(fname, 'r') as file:
            input_dict = yaml.safe_load(file)
    except Exception as e:
        raise Exception("Could not read file %s: %s" % (fname, e))

    try:
        validate(input_dict, schema)
    except Exception as e:
        raise Exception("Could not validate file %s with schema %s: %s" %
                        (fname, schema_fname, e))
    try:
        with open(str(fname) + '.without.defaults.yml', 'w') as file:
            yaml.dump(input_dict, file)
    except Exception as e:
        raise Exception("Could not write file %s.without.defaults.yml: %s" %
                        (fname, e))

    try:
        fill_in_defaults(input_dict, schema)
    except Exception as e:
        raise Exception("Could not fill in defaults: %s" % e)

    try:
        with open(str(fname) + '.with.defaults.yml', 'w') as file:
            yaml.dump(input_dict, file)
    except Exception as e:
        raise Exception("Could not write file %s.with.defaults.yml: %s" %
                        (fname, e))

    print()
    print("The original inputs were parsed and then dumped into %s.without.defaults.yml" % fname)
    print("After filling in defaults, the inputs dumped into    %s.with.defaults.yml" % fname)
    print("compare the two files to see which default values were added to your input\n")
    return input_dict
