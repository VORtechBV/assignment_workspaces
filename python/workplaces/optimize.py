import matplotlib.pyplot as plt
import numpy as np


def optimize_workspace(office, workplaces, show_plot=True):
    """
    Find the best place for the coffee machine and the toilet:
    Simply try all combinations of a location for the coffee machine and for the toilet and
    remember the best one.
    The search is finite because only points on a 2D grid of the office are tested.
    You can probably get slightly more optimal answers with a finer grid.
    """

    # Create a grid for the inside of the office
    x, y = office.getgrid()
    inside = office.check_inside(x, y)

    # Calculate the average distance from all work places to every spot in the office
    best = 0
    xy_toilet = None
    xy_coffee = None

    for i in np.arange(x.shape[0]):
        for j in np.arange(x.shape[1]):
            if inside[i, j]:
                sd = workplaces.shortest_path(xy_toilet=[x[i, j], y[i, j]], xy_coffee=[x, y],
                                              dist_method=office.measure)
                # Find the best spots for the coffee machine
                if np.max(sd[inside]) > best:
                    best = np.max(sd[inside])
                    xy_toilet = [x[i, j], y[i, j]]
                    ij = np.where(sd == best)
                    xy_coffee = [x[ij][0], y[ij][0]]

    return xy_coffee, xy_toilet, best


def show_shortest_path(office, workplaces, xy_coffee, xy_toilet):
    """
    The coffee machine is placed in all available locations (grid points), and for
    each point, the shortest path is calculated among the paths of all the employees in the office.
    """
    x, y = office.getgrid()
    inside = office.check_inside(x, y)

    best = workplaces.shortest_path(xy_toilet, xy_coffee, office.measure)

    plt.subplot(1, 1, 1)
    shortest_path = workplaces.shortest_path(xy_toilet=xy_toilet, xy_coffee=[x, y],
                                             dist_method=office.measure)
    shortest_path[np.logical_not(inside)] = np.NaN
    plt.contourf(x, y, shortest_path, levels=np.arange(0, best*31/30, best/30))
    plt.plot(xy_toilet[0], xy_toilet[1], 'go')
    plt.plot(xy_coffee[0], xy_coffee[1], 'g*')
    workplaces.plot()
    plt.title('Shortest walking distance for toilet at (%0.1f,%0.1f)' % (xy_toilet[0], xy_toilet[1]))
    office.plot()
    plt.gca().set_aspect('equal')
    plt.colorbar()
    plt.show()
