#!/usr/bin/env python3
import numpy as np

from workplaces.util import distance_method
from workplaces.work_places import WorkPlaces

FREQ_ONCE = 1.0
X1, Y1 = 12.0, 6.0
X_FAR, Y_FAR = 1200.0, 600.0
DIST = 3.7
NANGLE = 17


def test_work_places():
    """
    This test program tests the function shortest_path of the WorkPlaces class.

    The calculations are simplified by considering only the coffee machine and not the toilet.
    This is done by choosing the toilet-frequency to be freq_never = 0.0
    Four tests are conducted, with an increasing number of people in the WorkPlace object:

    1. just one person
    2. two persons, at the same location
    3. two persons at one location plus one very far away
    4. many persons, but at least one at the location of the coffee machine.

    In each test, the coffee machine is placed in locations on a circle around the first person.
    The shortest distance walked is:
    1. the answer is 2*the radius of the circle (to the coffee machine + from the coffee machine)
    2. the same answer
    3. the same answer
    4. the shortest distance is zero because at least one person is at the coffee machine itself.
    """
    places = WorkPlaces([])
    for itest in range(1, 5):
        print()
        if itest == 1:
            print('Test 1: just one person')
            places.add_place({"name": 'First person', 'x': X1, 'y': Y1,
                              'coffee_frequency': FREQ_ONCE})
        elif itest == 2:
            print('Test 2: add a person at the same location as First Person')
            places.add_place({"name": 'Second person', 'x': X1, 'y': Y1,
                              'coffee_frequency': FREQ_ONCE})
        elif itest == 3:
            print('Test 3: add a person very far from the First Person')
            places.add_place({"name": 'Third person', 'x': X_FAR, 'y': Y_FAR,
                              'coffee_frequency': FREQ_ONCE})
        elif itest == 4:
            print('Test 4: add a person at the coffee machine location')

        if itest <= 3:
            expected_distance = DIST * 2
        else:
            expected_distance = 0.0

        for iangle in range(NANGLE+1):
            angle = 2 * np.pi * iangle/NANGLE
            x_coffee = X1 + DIST * np.cos(angle)
            y_coffee = Y1 + DIST * np.sin(angle)
            if itest == 4:
                places.add_place({'name': 'Extra person', 'x': x_coffee, 'y': y_coffee,
                                  'coffee_frequency': FREQ_ONCE})
            distance = places.shortest_path(
                [x_coffee, y_coffee],
                [x_coffee, y_coffee],
                distance_method.EUCLIDEAN)
            if abs(expected_distance-distance) > 1e-10:
                print("For angle=%0.3g, (x,y)=(%0.3g,%0.3g), " % (angle, x_coffee, y_coffee),
                      'the calculated distance is %0.3g, but it should be %0.3g,' %
                      (distance, expected_distance),
                      'the error is %0.3g' % (distance - expected_distance))
                raise RuntimeError('incorrect result')
            else:
                print("For angle=%0.3g, (x,y)=(%0.3g,%0.3g), " % (angle, x_coffee, y_coffee),
                      'the calculated distance is %0.3g,' % (distance),
                      'the error is only %0.3g' % (distance - expected_distance))


if __name__ == '__main__':
    test_work_places()
