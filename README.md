# Assignment 'Workspaces'
<div align="center">
![Workspaces logo](figs/logo.jpeg "image Title")

_Logo of the assignment 'workspaces'_
</div>

## Scientific Software Engineering course
The assignment 'workspace' is part of the Scientific Software Engineering course taught by VORtech BV.

<div align="center">
![VORtech logo](figs/Vortech_logo.jpg "image Title")

_VORtech BV's logo_
</div>

## The assignment
The slides for lecture 1 are found at
https://vortechbv.gitlab.io/assignment_workspaces/01_Instruction_Morning_Day01.pdf

Assignment 1 is described in
https://vortechbv.gitlab.io/assignment_workspaces/memo19040.pdf

It shows you how to use __git__ to create, compare, and merge versions of a software system.

The slides for lecture 1 are found at
https://vortechbv.gitlab.io/assignment_workspaces/03_Instruction_Morning_Day02.pdf

Assignment 3 is described in
https://vortechbv.gitlab.io/assignment_workspaces/memo19042.pdf

It shows you how to use __git__ to differentiate various kinds of _software testing_.

The repository, https://gitlab.com/VORtechBV/assignment_workspaces.git
is used to illustrate these notions and allows you to practice.

## About the calculations done by program __workspaces__
The program __workspaces__ helps solve a problem that exists in every office environment: people do not move enough. The only reasons why they get off their chairs is to get coffee or to go to the toilet.

The program models the office workers' behavior, and then chooses optimal locations for the coffee machine and the toilet.

The optimal locations are defined by the distances walked by the people in the office, specifically the person who walks the __least__ of all the people in the office.  The optimal location makes this 'shortest distance' is as long as possible.

This program reads the input file, which is a yaml file for which a yaml schema is available.  
See 
[`the generated documentation`](https://vortechbv.gitlab.io/assignment_workspaces/workspace_input_options.html)
for information about the syntax of the input file.

The program

- creates the objects ( **office** and **places** ) representing the office and the people in it.
- Chooses the location for toilet and coffee machine.  The location is chosen by trying all possible combinations of 2 locations in a 2D grid of the office, and is therefore not computationally efficient.
- creates a plot to visualize the result.

## User Documentation

The documentation of the input file format is found at
https://vortechbv.gitlab.io/assignment_workspaces/workspace_input_options.html

## Building and running

### General installation
The program __workspaces__ is available both in fortran and in python.
Both use some python functionality: the fortran version uses python to validate and parse
the __yaml__ input file, and to generate graphical output. Before compiling
running the program, you will need to set up a python environment with the necessary modules.

To do this, you need a working __python__ installation with the module __venv__ available.

#### General installation on windows
If you have no __python__ on your computer, install it from
https://repo.anaconda.com/archive/Anaconda3-2022.05-Windows-x86_64.exe

We will run everything in a window called __git bash__ (which comes with your __git__ installation).

In the __git bash__ window, make sure that __python__ can be found by typing

```bash
export PATH=/c/ProgramData/Anaconda3/:$PATH
```

Next, install the necessary __python__ modules by typing

```bash
python -m venv workspaces-env
. workspaces-env/scripts/activate
python -m pip install -e python
```

#### General installation, on linux

If you don't have __python3__ with __venv__, but you _do_ have root privileges, you can install it using

```bash
sudo apt install python3-venv
```

Next, install the necessary __python__ modules by typing

```bash
python3 -m venv workspaces-env
. workspaces-env/bin/activate
python -m pip install -e python
```

#### Initialize the submodules (same on linux and windows)

Before you can build or run anything, make sure that you have te necessary _submodules_, by typing

```bash
git submodule update --init --recursive
```

### Building and running __workspaces__: python version

The python version does not need any compilation, and is ready for use. Start the program by typing

```bash
python ./python/workplaces/main.py files/invoer.workplaces.yml
```

or use an input file of you own making as the command line argument.

### Compiling the Fortran version of __workspaces__

For the compilation of the fortran version of __workspaces__, you need the programs __cmake__ and __gfortran__.

#### On Windows

1. Download and install
   [__cmake__](https://cmake.org/download/)
   and [__mingw__](https://sourceforge.net/projects/mingw/files/latest/download).
   While installing __mingw__, make sure to select __gfortran__ and __gmake__ for installation.
1. Let Windows know where to find the programs by adding them to your `PATH`.
   Type *environment variables* in the start menu, and open the program that pops up.
   Click *environment variables*. Select *PATH* en click *Edit*.
   Click *New* to add the path to MinGW (probably that's `C:\MinGW\bin`).
1. The required program __make__ is called __mingw32-make.exe__ in the __mingw64__ installation,
   which is difficult to type.
   Therefore make an alias in Git bash:

   ```bash
   alias make=mingw32-make.exe
   ```

   so that `make` is the same as `mingw32-make.exe`.

#### On Linux

If you don't have __cmake__ and/or __gfortran__ installed, but you _do_ have root privileges,
install them by typing

```bash
sudo apt install cmake gfortran
```

### Compiling fortran version of __workspaces__

- On Windows:

   ```bash
   (mkdir -p fortran/json-fortran/build && cd fortran/json-fortran/build && cmake -G "MinGW Makefiles" ../ && make -j)
   (mkdir -p fortran/build && cd fortran/build && cmake -G "MinGW Makefiles" ../../ && make -j all test)
   ```

- On Linux:

  ```bash
  (mkdir -p fortran/json-fortran/build && cd fortran/json-fortran/build && cmake ../ && make -j)
  (mkdir -p fortran/build && cd fortran/build && cmake ../../ && make -j all test)
  ```

### Running the Fortran version of __workspaces__

```bash
./fortran/build/main files/invoer.workplaces.yml
```

or use an input file of you own making as the command line argument.

## Code Documentation

The documentation of the **fortran** code is found at
[`https://vortechbv.gitlab.io/assignment_workspaces/fortran/dochtml/index.html`](https://vortechbv.gitlab.io/assignment_workspaces/fortran/dochtml/index.html)

The documentation of the **python** code is found at
[`https://vortechbv.gitlab.io/assignment_workspaces/dochtml/index.html`](https://vortechbv.gitlab.io/assignment_workspaces/dochtml/index.html).

## Static code analysis
The reports from various static analysers are found in
[`https://vortechbv.gitlab.io/assignment_workspaces/python/pyquality.html`](https://vortechbv.gitlab.io/assignment_workspaces/python/pyquality.html)

## Test Coverage

The test coverage of the fortran code is found at
[`https://vortechbv.gitlab.io/assignment_workspaces/fortran/coverage/coverage.html`](https://vortechbv.gitlab.io/assignment_workspaces/fortran/coverage/coverage.html).

The test coverage of the python code is found at
[`https://vortechbv.gitlab.io/assignment_workspaces/python/coverage/index.html`](https://vortechbv.gitlab.io/assignment_workspaces/python/coverage/index.html).
