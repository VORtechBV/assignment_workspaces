#!/usr/bin/env python3
import difflib
import os
import re
from glob import glob

with open('coverage.ok','r') as file:
     ok_lines = file.readlines()

ok_test_lines = sorted([re.sub('^ *','',line) for line in ok_lines if '####' in line])
ok_test_lines = [re.sub('#####: *[1-9][0-9]*:','',line) for line in ok_test_lines]

lines = []
for fname in glob(os.path.join('coverage','*.gcov')):
    with open(fname,'r') as file:
        lines += [fname + ':' + line for i,line in enumerate(file.readlines()) if '####' in line]

test_lines = [re.sub('#####: *[1-9][0-9]*:','',line) for line in sorted(lines)]

print("Differences between coverage output and verified coverage output:")
diff = difflib.context_diff(test_lines, ok_test_lines, fromfile='Untested lines', tofile='Untested lines OK',n=0)
for line in diff:
    print(line,end='')
