module yaml_mod
implicit none

   private
   public :: load_yaml   ! load the contents of a yaml file into a dict object (type(json_file))
   public :: get_value   ! get a value (integer, string or my_float) from a dict object

   interface get_value
      module procedure :: get_value__str
      module procedure :: get_value__bool
      module procedure :: get_value__int
      module procedure :: get_value__real
   end interface

contains

   ! First converts Yaml to JSON using Python
   ! then reads the JSON as a dict-like structure (type(json_file)), using an external lib.
   ! The temporary JSON file is not deleted.
   subroutine load_yaml(yaml_filepath, schema_file, dict)
   use json_file_module, only: json_file
   use LibUtil,          only: assert, endswith
      character(*),    intent(in) :: yaml_filepath, schema_file
      type(json_file), intent(out) :: dict

      logical :: is_ok
      character(:), allocatable :: json_filepath

      is_ok = endswith(yaml_filepath, '.yml')
      call assert(is_ok, "I expected a Yaml (i.e., '*.yml') file, but got: '" // yaml_filepath // "'")

      json_filepath = yaml_filepath(:len(yaml_filepath) - len('.yml')) // '-autoconverted.json'
      call yaml2json(yaml_filepath, schema_file, json_filepath)

      call dict%initialize( &
         strict_type_checking=.true., &
         trailing_spaces_significant=.true., &
         case_sensitive_keys=.false., &
         comment_char='/', &
         path_mode = 2, &     ! RFC 6901 format https://tools.ietf.org/html/rfc6901
      !                 http://fileformats.archiveteam.org/wiki/JSON#JSON_Pointer
         compress_vectors = .true., &
         allow_duplicate_keys =  .true., &
         stop_on_error = .false., &
         use_quiet_nan = .true., &
         strict_integer_type_checking = .true. &
         )
      call dict%load(filename=json_filepath)
   end subroutine

   subroutine get_value__str(dict, path, val, default, error_if_absent)
   use json_parameters,  only: json_string
   use json_file_module, only: json_file
   use LibUtil,          only: assert
      ! The 'default' need not be in the `choices` list.
      type(json_file), intent(inout) :: dict
      character(*), intent(in) :: path
      character(:), allocatable, intent(out) :: val
      character(*), optional, intent(in) :: default
      logical, optional, intent(in) :: error_if_absent

      logical :: found
      integer :: var_type
      logical :: local_error_if_absent

      local_error_if_absent = val_or_default(error_if_absent, default=.false.)

      if (present(default)) call assert(.not. local_error_if_absent, "get_value: bad call: present(default) and error_if_absent")

      call dict%info(path, found=found, var_type=var_type)
      if (local_error_if_absent) call assert(found, 'I was expecting this key path to be present, but it is not: ' // path)

      if (found) then
         call assert(var_type == json_string, 'I was expecting a string value in this key path: ' // path)

         call dict%get(path, val, found=found)
         call assert(found, "internal error: first I found this key path, and then I didn't?! path: " // path)

      elseif (present(default)) then
         val = default
      endif
   end subroutine

   subroutine get_value__bool(dict, path, val, found, error_if_absent)
   use json_parameters,  only: json_logical
   use json_file_module, only: json_file
   use LibUtil,          only: assert

      type(json_file), intent(inout) :: dict
      character(*), intent(in) :: path
      logical, intent(out) :: val
      logical, optional, intent(out) :: found
      logical, optional, intent(in) :: error_if_absent

      logical :: local_found
      integer :: var_type

      call dict%info(path, found=local_found, var_type=var_type)
      if (present(found)) found = local_found

      if (val_or_default(error_if_absent, default=.false.)) &
         call assert(local_found, 'I was expecting this key path to be present, but it is not: ' // path)

      if (local_found) then
         call assert(var_type == json_logical, 'I was expecting a boolean value in this key path: ' // path)

         call dict%get(path, val, found=local_found)
         call assert(local_found, "internal error: first I found this key path, and then I didn't?! path: " // path)
      endif
   end subroutine

   subroutine get_value__real(dict, path, val, found, error_if_absent)
   use json_parameters,  only: json_real
   use json_file_module, only: json_file
   use LibUtil,          only: assert
   use util,             only: my_float

      type(json_file), intent(inout) :: dict
      character(*), intent(in) :: path
      real(my_float), intent(out) :: val
      logical, optional, intent(out) :: found
      logical, optional, intent(in) :: error_if_absent

      logical :: local_found
      integer :: var_type

      call dict%info(path, found=local_found, var_type=var_type)
      if (present(found)) found = local_found

      if (val_or_default(error_if_absent, default=.false.)) &
         call assert(local_found, 'I was expecting this key path to be present, but it is not: ' // path)

      if (local_found) then
         call assert(var_type == json_real, 'I was expecting a float in this key path: ' // path)

         call dict%get(path, val, found=local_found)
         call assert(local_found, "internal error: first I found this key path, and then I didn't?! path: " // path)
      endif
   end subroutine

   subroutine get_value__int(dict, path, val, found, error_if_absent)
   use json_parameters,  only: json_integer
   use json_file_module, only: json_file
   use LibUtil, only: assert
      type(json_file), intent(inout) :: dict
      character(*), intent(in) :: path
      integer, intent(out) :: val
      logical, optional, intent(out) :: found
      logical, optional, intent(in) :: error_if_absent

      logical :: local_found
      integer :: var_type

      call dict%info(path, found=local_found, var_type=var_type)

      if (val_or_default(error_if_absent, default=.false.)) &
         call assert(local_found, 'I was expecting this key path to be present, but it is not: ' // path)

      if (present(found)) found = local_found

      if (local_found) then
         call assert(var_type == json_integer, 'I was expecting an integer value in this key path: ' // path)

         call dict%get(path, val, found=local_found)
         call assert(local_found, "internal error: first I found this key path, and then I didn't?! path: " // path)
      endif
   end subroutine


   subroutine yaml2json(yaml_file, schema_file, json_file)
   use LibUtil, only: assert, run_python
      character(*), intent(in) :: yaml_file, schema_file, json_file

      character(:), allocatable :: script
      character, parameter :: EOL = new_line('')

      call assert(scan(yaml_file // json_file, '"''' // EOL) == 0, &
         'yaml2json: I did not expect funny characters (like '' or " or an EOL) in a file name. yaml_file=' // yaml_file)

      ! Do NOT call an external script, because
      ! - that would add an external dependency (the script would
      !   have to remain unchanged in the same file path);
      ! - possible injection attacks would pose a serious security concern.
      script = "pass" &
         // EOL // "from workplaces.validate_and_defaults import read_input_with_defaults, dump_json " &
         // EOL // "yaml_object = read_input_with_defaults('"//trim(yaml_file)//"','"// &
                       trim(schema_file)//"')" &
         // EOL // "dump_json(yaml_object,'"//trim(json_file)//"')"

      call run_python(script)
   end subroutine

   logical function val_or_default(dummy, default)
      logical, optional, intent(in) :: dummy
      logical, intent(in) :: default

      if (present(dummy)) then
         val_or_default = dummy
      else
         val_or_default = default
      endif
   end function
end module
