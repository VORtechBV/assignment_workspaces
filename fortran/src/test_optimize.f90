! This program tests the optimization algorithm of program workspaces.
!
! The test runs a calculation in a variety of rotation angles and grid sizes.
! For each rotation angle, the sample input file is read, and then its contents are rotated
! The optimization is performed for a series of grid sizes, and every time, the
! optimal shortest distance is compared to the correct answer.
! Also the optimal locations for coffee machine and toilet are compared to the correct answer,
! after they have been rotated back to correspond to the original inputs.
!
! The answers are compared to the reference with an error tolerance that is reasonable given the grid sizes.
program test_optimize
use json_file_module, only: json_file
use yaml_mod,         only: load_yaml
use util,             only: my_float, EUCLIDEAN
use office_space,     only: OfficeSpace, init_office_from_dict, print_office
use work_places,      only: WorkPlaces, init_workplace_from_dict, print_workplace, shortest_path
use optimize,         only: optimize_workspace, show_shortest_path
implicit none
   type(json_file)   :: dict
   type(OfficeSpace) :: office
   type(WorkPlaces)  :: places
   real(my_float)    :: x_coffee, y_coffee, x_toilet, y_toilet
   character(len=20) :: arg
   character(len=100), parameter :: input_file = 'files/invoer.workplaces.yml'
   character(len=*), parameter :: schema_file = 'files/workplaces.schema.yml'
   integer :: show_plot
   real(my_float), parameter :: x_coffee_ref = 30, y_coffee_ref = 40
   real(my_float), parameter :: x_toilet_ref = 0, y_toilet_ref = 0
   real(my_float), parameter :: best_ref = 164
   real(my_float) :: angle
   real(my_float) :: best
   real(my_float) :: dx, dy
   real(my_float) :: x, y
   real(my_float), parameter :: pi = 3.14159265359d0
   integer,        parameter :: nangle = 7

   integer :: iangle
   integer :: i

   if (command_argument_count()==0) then
      show_plot = 0
   else if (command_argument_count()==1) then
      call get_command_argument(1,arg)
      read(arg,*) show_plot
   else
      stop 'call this program with one argument:' // &
           ' (show_plot=0:no plots, 1:plot for coarse grid, 2:all plots)'// &
           ' or none (default: 0)'
   end if

   do iangle = 0,nangle
      angle = 2 * pi * iangle/nangle

      ! Construct the objects
      call load_yaml(trim(input_file), schema_file, dict)
      call init_workplace_from_dict(places, dict)
      call init_office_from_dict(office, dict)

      ! Rotate the objects
      do i = 1,places%n_places
        x =  cos(angle) * places%x(i) + sin(angle) * places%y(i)
        y = -sin(angle) * places%x(i) + cos(angle) * places%y(i)
        places%x(i) = x
        places%y(i) = y
      end do

      do i = 1,size(office%xy,1)
        x =  cos(angle) * office%xy(i,1) + sin(angle) * office%xy(i,2)
        y = -sin(angle) * office%xy(i,1) + cos(angle) * office%xy(i,2)
        office%xy(i,1) = x
        office%xy(i,2) = y
      end do

      ! Print the objects
      print *
      print *,'work places:'
      call print_workplace(places)
      print *
      print *,'office:'
      call print_office(office)
      print *

      dx = 1.5
      dy = 1.7

      do i = 1,4

         dx = dx / 1.3
         dy = dy / 1.3

         print *
         print '(2(a,g0.4))', 'dx,dy=',dx,',',dy
         office%dx = dx
         office%dy = dy

         ! Optimize and show
         call optimize_workspace(office, places, x_coffee, y_coffee, x_toilet, y_toilet)
         print '(a,1p,2(g0.4,a))','The best place for the coffee machine is (',x_coffee,',',y_coffee,')'
         print '(a,1p,2(g0.4,a))','The best place for the toilet         is (',x_toilet,',',y_toilet,')'

         if ((show_plot>=1 .and. i==1) .or. show_plot>=2) then
            call show_shortest_path(office, places, x_coffee, y_coffee, x_toilet, y_toilet)
         end if

         print *,'coffee, toilet error = ', &
             abs(x_coffee_ref-x_coffee)+ abs(y_coffee_ref-y_coffee), &
             abs(x_toilet_ref-x_toilet)+ abs(y_toilet_ref-y_toilet)
         best = shortest_path( places, &
                               x_toilet=x_toilet, y_toilet=y_toilet, &
                               x_coffee=x_coffee, y_coffee=y_coffee, &
                               dist_method=EUCLIDEAN)
         print *,'shortest_path = ',best, (best-best_ref)/(dx + dy)**2



         ! Rotate the answers back
         x =  cos(angle) * x_coffee - sin(angle) * y_coffee
         y =  sin(angle) * x_coffee + cos(angle) * y_coffee
         x_coffee = x
         y_coffee = y

         if (max(abs(x_coffee_ref-x_coffee) + abs(y_coffee_ref-y_coffee), &
                 abs(x_toilet_ref-x_toilet) + abs(y_toilet_ref-y_toilet)) > 1.1*(dx + dy)) then
             print *,'coffee, toilet locations are too far from correct answer', &
             abs(x_coffee_ref-x_coffee), abs(y_coffee_ref-y_coffee), &
             abs(x_toilet_ref-x_toilet), abs(y_toilet_ref-y_toilet)
             stop 1
         end if

         if (abs(best-best_ref) > (dx+dy)**2) then
             print *,'shortest path error: ',best,'!=',best_ref
             stop 1
         end if


      end do
   end do
end program test_optimize

