! This module allows the office to be optimally designed, and produces plots to describe the
! optimization results
module optimize
use util, only: my_float
implicit none

   private
   public :: optimize_workspace ! Find the best place for the coffee machine and the toilet
   public :: show_shortest_path ! Graphical representation of optimized office lay-out


   logical,        parameter :: show_grid = .false.  ! Hard-coded input: could be moved to the input file
contains

   ! Find the best place for the coffee machine and the toilet:
   ! Simply try all combinations of a location for the coffee machine and for the toilet and
   ! remember the best one.
   ! The search is finite because only points on a 2D grid of the office are tested.
   ! You can probably get slightly more optimal answers with a finer grid.
   ! The grid specifications are stored in the object __OfficeSpace :: office__ .
   subroutine optimize_workspace(office, places, x_coffee, y_coffee, x_toilet, y_toilet)
   use office_space, only: OfficeSpace, getgrid, check_inside
   use work_places,  only: WorkPlaces, shortest_path

       type(OfficeSpace), intent(in)  :: office
       type(WorkPlaces),  intent(in)  :: places
       real(my_float),    intent(out) :: x_coffee, y_coffee, x_toilet, y_toilet

       real(my_float), allocatable :: x(:,:), y(:,:)
       logical,        allocatable :: inside(:,:)
       real(my_float) :: best
       real(my_float) :: sd
       integer :: i, j
       integer :: i1, j1

       ! Create a grid for the inside of the office
       call getgrid(office, x, y)
       call check_inside(office, x, y, inside)

       ! Calculate the average distance from all work places to every spot in the office
       best = 0

       ! Brute force: just try all possible combinations of coffee machine and toilet locations
       do j = 1,size(x,2)
          do i = 1,size(x,1)
             if (.not. inside(i,j)) cycle

             ! Find the best spots for the coffee machine and the toilet
             do j1 = 1,size(x,2)
                do i1 = 1,size(x,1)
                   if (.not. inside(i1,j1)) cycle
                   sd = shortest_path( places, x_toilet = x(i,j), y_toilet = y(i,j), &
                                               x_coffee = x(i1,j1), y_coffee = y(i1,j1), &
                                               dist_method = office%measure)
                   if (sd > best) then
                      best = sd
                      x_toilet = x(i,j)
                      y_toilet = y(i,j)
                      x_coffee = x(i1,j1)
                      y_coffee = y(i1,j1)
                   end if
                end do
             end do
          end do
       end do
   end subroutine optimize_workspace


   ! The coffee machine is placed in all available locations (grid points), and for
   ! each point, the shortest path is calculated among the paths of all the employees in the office.
   ! Thus, the contour plot shows the shortest path as a function of the coffee machine location.
   subroutine show_shortest_path(office, places, x_coffee, y_coffee, x_toilet, y_toilet)
   use libUtil, only: run_python
   use util, only: nan
   use office_space, only: OfficeSpace, getgrid, check_inside, office_plotstring
   use work_places,  only: WorkPlaces, shortest_path, workplace_plotstring
       type(OfficeSpace), intent(in) :: office ! The office in which to show the shortest paths
       type(WorkPlaces),  intent(in) :: places ! The workplaces of the employees
       real(my_float),    intent(in) :: x_coffee, y_coffee ! Location (x,y) of the coffee machine [m]
       real(my_float),    intent(in) :: x_toilet, y_toilet ! Location (x,y) of the toilet [m]

       real(my_float), allocatable :: x(:,:), y(:,:)
       logical,        allocatable :: inside(:,:)
       real(my_float) :: best
       real(my_float), allocatable :: path(:,:)
       integer :: i1, i2, n1, n2
       character(len=100000) :: xstr, ystr, path1str, path2str, wplotstr, ofplotstr
       character(len=1000000) :: script
       character, parameter :: EOL = new_line('')

       ! Create a grid for the inside of the office
       call getgrid(office, x, y)
       call check_inside(office, x, y, inside)
       n1 = size(x,1)
       n2 = size(x,2)
       allocate(path(n1,n2))

       best = shortest_path( places, &
                             x_toilet=x_toilet, y_toilet=y_toilet, &
                             x_coffee=x_coffee, y_coffee=y_coffee, &
                             dist_method = office%measure)
       do i2 = 1,n2
          do i1 = 1,n1
             if (inside(i1,i2)) then
                 path(i1,i2) = shortest_path( places, &
                                 x_toilet=x_toilet, y_toilet=y_toilet, &
                                 x_coffee=x(i1,i2), y_coffee=y(i1,i2), &
                                 dist_method = office%measure)
             else
                 path(i1,i2) = nan
             endif
          end do
       end do

       write(xstr,'(10000(a,g0.5))') &
            '[', &
            ( x(i1,1), (',', x(i1,i2), i2=2,n2), '],' // EOL // '[', i1=1,n1-1), &
            x(n1,1), (',', x(n1,i2), i2=2,n2), ']' // EOL
       write(ystr,'(10000(a,g0.5))') &
            '[', &
            ( y(i1,1), (',', y(i1,i2), i2=2,n2), '],' // EOL // '[', i1=1,n1-1), &
            y(n1,1), (',', y(n1,i2), i2=2,n2), ']' // EOL
       write(path1str,'(10000(a,g0.5))') &
            '[', &
            ( path(i1,1), (',', path(i1,i2), i2=2,n2), '],' // EOL // '[', i1=1,n1-1), &
            path(n1,1), (',', path(n1,i2), i2=2,n2), ']' // EOL

       call workplace_plotstring(places, wplotstr)
       call office_plotstring(office, show_grid, ofplotstr)

       write(script,'(100(a,g0.3))') &
          "import numpy as np" // EOL // &
          "import matplotlib.pyplot as plt" // EOL // &
          "NaN = np.nan" // EOL // &
          "x = ["//trim(xstr)//"]" // EOL // &
          "y = ["//trim(ystr)//"]" // EOL // &
          "path1 = ["//trim(path1str)//"]" // EOL // &
          "ax = plt.subplot(1,1,1)" // EOL // &
          "plt.contourf(x,y,path1, levels=np.arange(0,",best*31/30,',',best/30,"))" // EOL // &
          "plt.plot(",x_toilet,',',y_toilet,',''go'')' // EOL // &
          "plt.plot(",x_coffee,',',y_coffee,',''g*'')' // EOL // &
          trim(wplotstr) // &
          trim(ofplotstr) // &
          "plt.title('Shortest walking distance for toilet at (",x_toilet,',',y_toilet,")')" // EOL // &
          "plt.colorbar()" // EOL // &
          "plt.gca().set_aspect('equal')" // EOL // &
          "plt.show()"

       call run_python(script)

   end subroutine show_shortest_path

end module optimize

