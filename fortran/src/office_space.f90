! This module offers the class OfficeSpace, with some methods
! The class describes the shape of an office, plus some information about how it should be gridded.
module office_space
use util, only: my_float, EUCLIDEAN
implicit none

    private
    public :: OfficeSpace           ! The class (derived type)
    public :: init_office_from_dict ! Constructor
    public :: print_office          ! Output function
    public :: check_inside          ! Check which points are inside and which are outside the office
    public :: getgrid               ! Create a uniform 2D grid for the office
    public :: office_plotstring     ! Return code snippet that can be passed to python for plotting
    public :: init                  ! Return code snippet that can be passed to python for plotting

    integer, parameter :: MINIMUM = 1, AVERAGE = 2

    ! Describe the office space as a 2D shape.
    type OfficeSpace
        integer :: measure                       ! Either EUCLIDEAN or MANHATTAN: distance measure used.
        real(my_float), allocatable :: xy(:,:)   ! Contours of the office
        real(my_float) :: dx, dy                 ! Grid spacing
    end type OfficeSpace

contains

    ! Read the field __/office_contours/__ from the yaml-input file to initialize the OfficeSpace
    ! object.
    subroutine init_office_from_dict(space, dict)
    use util,             only: EUCLIDEAN, MANHATTAN
    use json_file_module, only: json_file
    use libUtil,          only: i2a
    use yaml_mod,         only: get_value

       type(json_file),   intent(inout) :: dict ! contents of the yaml input file
       type(OfficeSpace), intent(out)   :: space

       logical :: found
       integer :: var_type, n_children, i_child
       real(my_float), allocatable :: xy(:,:)
       real(my_float) :: x, y
       character(:), allocatable :: number
       character(:), allocatable :: smeasure
       integer :: measure

       call get_value(dict, '/office/measure', smeasure, error_if_absent=.true.)
       if (smeasure == 'EUCLIDEAN') then
          measure = EUCLIDEAN
       else
          measure = MANHATTAN
       end if
       call dict%info('/office/contours', found=found, var_type=var_type, n_children=n_children)
       allocate(xy(n_children,2))
       do i_child = 1,n_children
          number = i2a(i_child-1)
          call dict%info('/office/contours/'// number, found=found, var_type=var_type, n_children=n_children)
          call get_value(dict, '/office/contours/' // number // '/0', x, error_if_absent=.true.)
          call get_value(dict, '/office/contours/' // number // '/1', y, error_if_absent=.true.)
          xy(i_child,1) = x
          xy(i_child,2) = y
       end do
       call init(space, measure, xy(:,1), xy(:,2), dx=1d0, dy=1d0)
       deallocate(xy)
    end subroutine init_office_from_dict



    subroutine print_office(space)
    use util, only: EUCLIDEAN
        type(OfficeSpace), target, intent(in) :: space

        integer :: ixy, nxy
        real(my_float), pointer :: x(:), y(:)

        x => space%xy(:,1)
        y => space%xy(:,2)
        nxy = size(x,1)

        if (space%measure == EUCLIDEAN) then
           print '(a)', 'measure: EUCLIDEAN'
        else
           print '(a)', 'measure: MANHATTAN'
        end if
        print '(a,1p,2(g0.2,a))','   sampling: (',space%dx,',',space%dy,') [m]'
        print '(a)','   contours:'
        do ixy = 1,nxy
           print '(a,1p,2(g0.2,a))','          (',x(ixy),',',y(ixy),')'
        end do
    end subroutine print_office


    subroutine init(space, measure, x, y, dx, dy)
        type(OfficeSpace), intent(out) :: space
        integer,        intent(in) :: measure
        real(my_float), intent(in) :: x(:), y(:)
        real(my_float), intent(in) :: dx, dy
        integer :: ixy, nxy
        space%measure = measure
        space%dx = dx
        space%dy = dy
        nxy = size(x,1)

        if (allocated(space%xy)) deallocate(space%xy)

        allocate(space%xy(nxy,2))
        do ixy = 1,nxy
           space%xy(ixy,1) = x(ixy)
           space%xy(ixy,2) = y(ixy)
        end do
    end subroutine init




    logical function check_inside_one_point(space, x_grid, y_grid)
    ! Check whether location (x_grid, y_grid) is inside the office
        type(OfficeSpace), target, intent(in) :: space
        real(my_float), intent(in) :: x_grid, y_grid

        real(my_float), pointer :: x(:), y(:)
        real(my_float) :: alpha, beta, det
        real(my_float) :: xstart, xend, ystart, yend
        integer        :: ixy, nxy, n_crossings

        x => space%xy(:,1)
        y => space%xy(:,2)
        nxy = size(x,1)

        n_crossings = 0
        do ixy= 1,nxy-1
           xstart = x(ixy)
           ystart = y(ixy)

           xend = x(ixy+1)
           yend = y(ixy+1)

           det = (ystart-yend)
           alpha = ( (ystart-yend) * (xstart - x_grid) + (xend-xstart)*(ystart-y_grid) )/det
           beta  = (ystart-y_grid) / det

           if (alpha>0 .and. beta>0 .and. beta<1) n_crossings = n_crossings + 1

        end do
        check_inside_one_point = mod(n_crossings, 2) == 1
    end function check_inside_one_point



    subroutine check_inside(space, x_grid, y_grid, inside)
    ! Check whether locations (x_grid, y_grid) are inside the office
        type(OfficeSpace), target,   intent(in)  :: space
        real(my_float),              intent(in)  :: x_grid(:,:), y_grid(:,:)
        logical,        allocatable, intent(out) :: inside(:,:)

        integer :: i1, i2

        allocate(inside(size(x_grid,1), size(x_grid,2)))
        do i2 = 1,size(x_grid,2)
           do i1 = 1,size(x_grid,1)
              inside(i1,i2) = check_inside_one_point(space, x_grid(i1,i2), y_grid(i1,i2))
           end do
        end do
    end subroutine check_inside



    subroutine getgrid(space, x_grid, y_grid)
    ! Return x,y coordinates that define a grid on the office floor
        type(OfficeSpace), target,   intent(in)  :: space
        real(my_float), allocatable, intent(out) :: x_grid(:,:), y_grid(:,:)

        real(my_float), pointer :: x(:), y(:)
        real(my_float) :: dx, dy
        real(my_float) :: xmin, xmax, ymin, ymax
        integer        :: nxy
        integer        :: i1, i2, n1, n2


        x => space%xy(:,1)
        y => space%xy(:,2)
        nxy = size(x,1)

        dx = space%dx
        dy = space%dy
        xmin = minval(x)
        xmax = maxval(x)
        ymin = minval(y)
        ymax = maxval(y)
        n1 = ceiling( (xmax - (xmin+dx/2) ) / dx )
        n2 = ceiling( (ymax - (ymin+dy/2) ) / dy )

        allocate(x_grid(n1, n2), y_grid(n1, n2))
        do i2 = 1,n2
           do i1 = 1,n1
              x_grid(i1,i2) = xmin + dx * (i1-0.5)
              y_grid(i1,i2) = ymin + dy * (i2-0.5)
           end do
        end do
    end subroutine getgrid

    subroutine office_plotstring(space, plot_grid, plotstring)
    ! Plot the office (and the grid) in the current figure
        type(OfficeSpace), target,   intent(in)  :: space
        logical,                     intent(in)  :: plot_grid
        character(len=*),            intent(out) :: plotstring

        logical,        allocatable :: inside(:,:)
        real(my_float), allocatable :: x_grid(:,:), y_grid(:,:)
        character, parameter :: EOL = new_line('')
        integer :: i, i1, i2, n1, n2
        write(plotstring,'(1000(a,g0.3))') &
           'x_office = [', space%xy(1,1), ( ',', space%xy(i,1), i=2, size(space%xy,1)), ']' // EOL // &
           'y_office = [', space%xy(1,2), ( ',', space%xy(i,2), i=2, size(space%xy,1)), ']' // EOL // &
           "plt.plot(x_office,y_office,'k')" // EOL

        if (plot_grid) then
           call getgrid(space, x_grid, y_grid)
           call check_inside(space, x_grid, y_grid, inside)
           n1 = size(x_grid,1)
           n2 = size(x_grid,2)
           do i2 = 1,n2
              do i1 = 1,n1
                 if (inside(i1,i2)) then
                    write(plotstring,'(1000(a,g0.3))') &
                         trim(plotstring) // EOL // &
                         "plt.plot(",x_grid(i1,i2),',',y_grid(i1,i2),",'b.')" // EOL
                 end if
              end do
           end do
        end if

    end subroutine office_plotstring

end module office_space

