
! This program aims to help solve a problem which exists in every office environment:
! people do not move enough. The only reasons why they get off their chairs is to get coffee
! or to go to the toilet.
!
! The program models the office workers' behavior, and then chooses an optimal location for
! the coffee machine and for the toilet.
!
! The optimal locations are defined by the distances walked by the people in the office,
! specifically the person who walks the __least__ of all the people in the office.
! The optimal location makes this 'shortest distance' is as long as possible.
!
! This program reads the input file, which is a yaml file for which a yaml schema is available.
! See the generated documentation for information about the syntax of the input file.
!
! The program
! + creates the objects ( __OfficeSpaces :: office__ , __WorkPlaces:: places__ ) representing
!   the office and the people in it.
! + Chooses the location for toilet and coffee machine.
!   The location is chosen by trying all possible combinations of 2 locations in a
!   2D grid of the office, and is therefore not computationally efficient.
! + creates a plot to visualize the result.
program main
use json_file_module, only: json_file
use yaml_mod,         only: load_yaml
use util,             only: my_float
use office_space,     only: OfficeSpace, init_office_from_dict, print_office
use work_places,      only: WorkPlaces, init_workplace_from_dict, print_workplace
use optimize,         only: optimize_workspace, show_shortest_path
implicit none
   type(json_file)   :: dict
   type(OfficeSpace) :: office
   type(WorkPlaces)  :: places
   real(my_float)    :: x_coffee, y_coffee, x_toilet, y_toilet
   character(len=*), parameter :: schema_file = 'files/workplaces.schema.yml'
   character(len=100) :: input_file

   ! Choose the name of the input file
   if (command_argument_count()==0) then
      input_file = 'files/invoer.workplaces.yml'
   else if (command_argument_count()==1) then
      call get_command_argument(1,input_file)
   else
      stop 'call this program with one argument (input file) or none (default: files/invoer.workplaces.yml)'
   end if

   ! Construct the objects
   call load_yaml(trim(input_file), schema_file, dict)
   call init_workplace_from_dict(places, dict)
   call init_office_from_dict(office, dict)

   ! Print the objects
   print *
   print *,'work places:'
   call print_workplace(places)
   print *
   print *,'office:'
   call print_office(office)
   print *

   ! Optimize and show
   call optimize_workspace(office, places, x_coffee, y_coffee, x_toilet, y_toilet)
   print '(a,1p,2(g0.2,a))','The best place for the coffee machine is (',x_coffee,',',y_coffee,')'
   print '(a,1p,2(g0.2,a))','The best place for the toilet         is (',x_toilet,',',y_toilet,')'
   call show_shortest_path(office, places, x_coffee, y_coffee, x_toilet, y_toilet)

end program main
