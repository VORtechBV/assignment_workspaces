! This test program tests the function shortest_path of the WorkPlaces class.
!
! The calculations are simplified by considering only the coffee machine and not the toilet.
! This is done by choosing the toilet-frequency to be freq_never = 0.0
! Four tests are conducted, with an increasing number of people in the WorkPlace object:
!
! 1. just one person
! 2. two persons, at the same location
! 3. two persons at one location plus one very far away
! 4. many persons, but at least one at the location of the coffee machine.
!
! In each test, the coffee machine is placed in locations on a circle around the first person.
! The shortest distance walked is:
! 1. the answer is 2*the radius of the circle (to the coffee machine + from the coffee machine)
! 2. the same answer
! 3. the same answer
! 4. the shortest distance is zero because at least one person is at the coffee machine itself.
!
program test_work_places
use util, only: my_float
use work_places, only: WorkPlaces, add_place, shortest_path
use util, only: EUCLIDEAN

    type(WorkPlaces) :: places

    real(my_float), parameter :: freq_once = 1.0
    real(my_float), parameter :: x1 = 12.0, y1 = 6.0
    real(my_float), parameter :: x_far = 1200.0, y_far = 600.0
    real(my_float), parameter :: pi = 3.14159265359d0
    real(my_float), parameter :: dist = 3.7
    integer,        parameter :: nangle = 17

    real(my_float)  :: expected_distance
    real(my_float)  :: distance
    real(my_float)  :: x_coffee, y_coffee
    real(my_float)  :: angle
    integer         :: iangle, itest



    do itest = 1,4
       print *
       if (itest == 1) then
          print '(a)', 'Test 1: just one person'
          call add_place(places, 'First person', x1, y1, freq_once)
       else if (itest==2) then
          print '(a)', 'Test 2: add a person at the same location as First Person'
          call add_place(places, 'Second person', x1, y1, freq_once)
       else if (itest==3) then
          print '(a)', 'Test 3: add a person very far from the First Person'
          call add_place(places, 'Third person', x_far, y_far, freq_once)
       else if (itest==4) then
          print '(a)', 'Test 4: add a person at the coffee machine location'
       end if

       if (itest<=3) then
          expected_distance = dist * 2
       else
          expected_distance = 0.0
       end if

       do iangle = 0,nangle
          angle = 2 * pi * iangle/nangle
          x_coffee = x1 + dist * cos(angle)
          y_coffee = y1 + dist * sin(angle)
          if (itest == 4) then
             call add_place(places, 'Extra person', x_coffee, y_coffee, freq_once)
          end if
          distance = shortest_path(places, &
                                       x_coffee, y_coffee, &
                                       x_coffee, y_coffee, &
                                       EUCLIDEAN)
          if (abs(expected_distance-distance) > 1e-10) then
              print '(1p,5(a,g0.3),a,e10.2)', &
                   "For angle=",angle,', (x,y)=',x_coffee,',',y_coffee,'), '// &
                   'the calculated distance is ',distance,', but it should be ',expected_distance, &
                   ', the error is ',distance - expected_distance
              stop 1
          else
              print '(1p,4(a,g0.3),a,e10.2)', &
                   "For angle=",angle,', (x,y)=',x_coffee,',',y_coffee,'), '// &
                   'the calculated distance is ',distance, &
                   ', the error is only ',distance - expected_distance
          end if
       end do
    end do
end program test_work_places

