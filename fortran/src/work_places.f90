!
module work_places
use util, only: my_float
implicit none

integer, parameter :: MXLEN = 80, MXNPLACES = 200
integer, parameter :: AVERAGE = 1, MINIMUM = 2


   ! Describe the work places of the workers in an office
   ! Each work place has the name of whose spot it is and (x,y) coordinates
   type WorkPlaces
        character(len=MXLEN) :: names(MXNPLACES)
        real(my_float)       :: x(MXNPLACES), y(MXNPLACES)
        real(my_float)       :: coffee_freq(MXNPLACES)
        integer              :: n_places = 0
   end type WorkPlaces

contains

    ! Read the field __/workplaces/__ from the yaml-input file to initialize the OfficeSpace
    ! object.
    subroutine init_workplace_from_dict(places, dict)
    use json_file_module, only: json_file
    use libUtil,          only: i2a
    use yaml_mod,         only: get_value

       type(json_file),  intent(inout)  :: dict
       type(WorkPlaces), intent(out) :: places

       logical :: found
       integer :: var_type, n_children, i_child
       character(:), allocatable :: number
       character(:), allocatable :: name
       real(my_float) :: x, y
       real(my_float) :: coffee_freq

       places%n_places = 0
       call dict%info('/workplaces', found=found, var_type=var_type, n_children=n_children)

       do i_child = 1,n_children
          number = i2a(i_child-1)
          call get_value(dict, '/workplaces/' // number // '/name', name, error_if_absent=.true.)
          call get_value(dict, '/workplaces/' // number // '/x', x, error_if_absent=.true.)
          call get_value(dict, '/workplaces/' // number // '/y', y, error_if_absent=.true.)
          call get_value(dict, '/workplaces/' // number // '/coffee_frequency', coffee_freq, error_if_absent=.true.)
          call add_place(places, name, x, y, coffee_freq)
       end do
    end subroutine init_workplace_from_dict


    subroutine print_workplace(places)
       type(WorkPlaces), intent(in) :: places

       integer :: i_place

       do i_place = 1,places%n_places
          print '(a,1p,2(g0.2,a))','    ' // places%names(i_place)(1:10) // '(',places%x(i_place),',',places%y(i_place),')'
       end do
    end subroutine print_workplace

    subroutine add_place(places, name, x, y, coffee_freq)
       type(WorkPlaces), intent(inout) :: places
       character(len=*), intent(in)    :: name
       real(my_float),   intent(in)    :: x, y
       real(my_float),   intent(in)    :: coffee_freq

       places%n_places = places%n_places + 1
       places%names(places%n_places) = name
       places%x(    places%n_places) = x
       places%y(    places%n_places) = y
       places%coffee_freq(    places%n_places) = coffee_freq
    end subroutine add_place

    real(my_float) function walking_distance(places, x_toilet, y_toilet, x_coffee, y_coffee, &
                                                 reduction, method) result(retval)
    ! Calculate the distance walked by all the workers in a day
    ! Return the minimum/average value, depending on the chosen reduction
    use util, only: distance
        type(WorkPlaces), intent(in) :: places
        real(my_float),   intent(in) :: x_toilet, y_toilet
        real(my_float),   intent(in) :: x_coffee, y_coffee
        integer,          intent(in) :: reduction, method

        integer :: iplace
        real(my_float) :: dist

        retval = 0
        if (reduction == MINIMUM) retval = 1e30

        do iplace = 1, places%n_places
           dist = 2d0/3d0*places%coffee_freq(iplace) * 2 * &
                                  distance( x_coffee, y_coffee, places%x(iplace), places%y(iplace), method) + &
                  1d0/3d0*places%coffee_freq(iplace) * &
                              (   distance( x_coffee, y_coffee, places%x(iplace), places%y(iplace), method) + &
                                  distance( x_coffee, y_coffee, x_toilet,         y_toilet        , method) + &
                                  distance( x_toilet, y_toilet, places%x(iplace), places%y(iplace), method)   )
           if (reduction == AVERAGE) then
              retval = retval + dist / places%n_places
           else if (reduction == MINIMUM) then
              retval = min(retval,dist)
           else
              stop ('unknown reduction method')
           end if

        end do
    end function walking_distance

    real(my_float) function shortest_path(places, x_toilet, y_toilet, x_coffee, y_coffee, dist_method)
    ! Calculate the shortest path for all the workers to each of the given points (x,y)
        type(WorkPlaces), intent(in) :: places
        real(my_float),   intent(in) :: x_toilet, y_toilet
        real(my_float),   intent(in) :: x_coffee, y_coffee
        integer,          intent(in) :: dist_method
        shortest_path = walking_distance( places, x_toilet, y_toilet, x_coffee, y_coffee, &
                                              reduction=MINIMUM, method=dist_method )
    end function shortest_path

    ! Calculate the average distance from all the workers to each of the given points
    real(my_float) function average_distance(places, x_toilet, y_toilet, x_coffee, y_coffee, dist_method)
        type(WorkPlaces), intent(in) :: places
        real(my_float),   intent(in) :: x_toilet, y_toilet
        real(my_float),   intent(in) :: x_coffee, y_coffee
        integer,          intent(in) :: dist_method
        average_distance = walking_distance( places, x_toilet, y_toilet, x_coffee, y_coffee, &
                                                 reduction=AVERAGE, method=dist_method )
    end function average_distance

    subroutine workplace_plotstring(places, pstr)
    ! Plot cyan stars at the work places in the current figure
        type(WorkPlaces), intent(in) :: places
        character(len=*), intent(out) :: pstr
        character, parameter :: EOL = new_line('')
        integer :: iplace
        write(pstr,'(100(a,g0.3,a,g0.3,a))') &
            ("plt.plot(",places%x(iplace),',',places%y(iplace),',''c*'')' // EOL , &
            iplace = 1, places%n_places)
    end subroutine workplace_plotstring

end module work_places

