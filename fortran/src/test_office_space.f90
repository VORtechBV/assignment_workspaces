! This test program tests the functions getgrid and check_inside of the class OfficeSpace.
! To do this, an office is defined with a known area, equal to 136 m2.
! For a series of grid sizes, the area is approximated by adding the areas of grid cells inside
! the office.
! The calculated area is compared with an error tolerance that is reasonable given the grid sizes.
program test_office_space
use libUtil, only: run_python
use util, only: my_float, EUCLIDEAN
use office_space, only: OfficeSpace, init, getgrid, check_inside, office_plotstring
implicit none
   type(OfficeSpace) :: space
   integer, parameter :: n_points = 6
   real(my_float), parameter :: x(n_points) = (/0.0, 10.0, 10.0,  0.0, 3.0, 0.0/)
   real(my_float), parameter :: y(n_points) = (/0.0, 0.0,  16.0, 16.0, 8.0, 0.0/)
   real(my_float) :: dx, dy
   real(my_float), allocatable :: x_grid(:,:), y_grid(:,:)
   integer :: i, n_inside, ix, iy
   character(len=200000) :: plotstring
   logical,        allocatable :: inside(:,:)
   real(my_float), parameter :: correct_area = 136.0
   real(my_float) :: area, acceptable_error
   logical, parameter :: do_plot = .false.

   do i = 1,6
      dx = 0.43 * i
      dy = 0.37 * i

      call init(space, EUCLIDEAN, x,y, dx, dy)
      call getgrid(space, x_grid, y_grid)
      call check_inside(space, x_grid, y_grid, inside)

      n_inside = 0
      do iy = 1, size(x_grid,2)
         do ix = 1, size(x_grid,1)
             if (inside(ix,iy)) n_inside = n_inside + 1
         end do
      end do

      print '(1p,2(a,g0.3),3(a,i0),10(a,g0.3))', &
         '(dx,dy)=(',dx,',',dy,'): '// &
         '(nx,ny) = (',size(x_grid,1),',',size(x_grid,2),'), ', &
         n_inside,' points inside the office, office is ',dx*dy*n_inside ,' m2'

      area = dx*dy*n_inside

      acceptable_error = dx * 16 + dy * 10
      print '(1p,10(a,g0.3))','error in area = ', (area - correct_area)/acceptable_error , ' times the acceptable error'
      if ( abs(area-correct_area) > acceptable_error ) then
         print *,'error is too large'
         stop 1
      end if

      if (do_plot) then
         call office_plotstring(space, plot_grid=.true., plotstring=plotstring)
         call run_python('import matplotlib.pyplot as plt' // new_line('') // trim(plotstring) // 'plt.show()' // new_line(''))
      end if

      deallocate(x_grid, y_grid, inside)
   end do
end program test_office_space
