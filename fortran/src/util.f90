module util
use, intrinsic :: iso_fortran_env, only: real64, int64
implicit none

   private
   public :: EUCLIDEAN, MANHATTAN  ! Two types of distance-notions
   public :: nan                   ! Not a Number
   public :: my_float              ! Data type used for floating point numbers
   public :: distance              ! The distance function, according to the selected type

   integer,        parameter :: EUCLIDEAN = 1, MANHATTAN = 2
   integer,        parameter :: my_float = real64
   real(my_float), parameter :: nan =  transfer(-2251799813685248_int64, 1._real64)

contains

   ! Calculate the distances between (x1,y1) and (x2,y2)
   ! according to the chosen method (euclidean or manhattan)
   real(my_float) function distance(x1,y1,x2,y2, method)
       real(my_float), intent(in)  :: x1, y1, x2, y2
       integer,        intent(in)  :: method

       if (method == EUCLIDEAN) then
          distance = sqrt( (x1-x2)**2 + (y1-y2)**2 )
       else if (method == MANHATTAN) then
          distance = abs(x1-x2) + abs(y1-y2)
       else
          stop ("method should be MANHATTAN or EUCLIDEAN")
       end if
   end function distance

end module util

